<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chat GPT Laravel</title>

<!-- Javascript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>


<!-- Style -->
<link rel="stylesheet" href="/style.css">


</head>

<body>
    <div class="chat">

    <!-- Header -->
    <div class="top">
        <div>
            <p>Wisnu Dewantara</p>
            <small>online</small>            
        </div>        
    </div>
    <!-- EndHeader -->

    <!-- Chat Body -->
    <div class="messages">
        <div class="left message">
            <img src="{{ asset('img/PP.jpg') }}" alt="Avatar">
            <p>Start Chatting</p>             
        </div>
    </div>
    <!-- End Chat Body -->

    <!-- Footer -->
    <div class="bottom">
        <form>
            <input type="text" id="message" name="message" placeholder="Enter message. . . " autocomplete="off">
            <button type="submit"></button>
        </form>

    </div>
    <!-- End Footer -->

</div>
</body>

<script>
    //Submit messages
    $("form").submit(function (event){
        event.preventDefault();

        //Stop empty messages
        if ($("form #message").val().trim() === '') {
            return;
        }

        //Disable duplicate
        $("form #message").prop('disabled', true);
        $("form button").prop('disabled', true);
        
        $.ajax({
            url:"/chat",
            method:'POST',
            headers:{
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
            data: {
                "model": "gpt-4-1106-preview",
                "content": $("form #message").val()
            }
        }).done(function (res) {

      //Populate sending message
      $(".messages > .message").last().after('<div class="right message">' +
        '<p>' + $("form #message").val() + '</p>' +
        '<img src="{{ asset('img/PP.jpg') }}" alt="Avatar">' +
        '</div>');

      //Populate receiving message
      $(".messages > .message").last().after('<div class="left message">' +
        '<img src="{{ asset('img/PP.jpg') }}" alt="Avatar">' +
        '<p>' + res + '</p>' +
        '</div>');

      //Cleanup
      $("form #message").val('');
      $(document).scrollTop($(document).height());

      //Enable form
      $("form #message").prop('disabled', false);
      $("form button").prop('disabled', false);
    });
  });

</script>
</html>
